# Lesson 1 - using arcade.

import arcade

def artWindow(size, step):

    arcade.open_window('Art Window', size, size)

    arcade.set_background_color(arcade.color.ANTI_FLASH_WHITE)

    arcade.start_render()

    for ii in range(0,size,step):
        start_x = ii
        start_y = 0
        end_x = size
        end_y = ii
        arcade.draw_line(start_x, start_y, end_x, end_y, arcade.color.BLACK)

    for ii in range(0,size,step):
        start_x = size - ii
        start_y = 0
        end_x = 0
        end_y = ii
        arcade.draw_line(start_x, start_y, end_x, end_y, arcade.color.RED)

    for ii in range(0, size, step):
        start_x = ii
        start_y = size
        end_x = 0
        end_y = ii
        arcade.draw_line(start_x, start_y, end_x, end_y, arcade.color.BLACK)

    for ii in range(0, size, step):
        start_x = ii
        start_y = size
        end_x = size
        end_y = size - ii
        arcade.draw_line(start_x, start_y, end_x, end_y, arcade.color.RED)


     # TODO - draw the other two sides of the figure.



    # TODO - ADVANCED
    #
    # Make a seperate function that draws the pattern within a box
    # with given coordinates, then call it for several different
    # locations and sizes from within this function.

    arcade.finish_render()

    arcade.run()

size = 400
step = 7

artWindow(size, step)



