## Python Weight Room 1

Becoming a good coder is like becoming a good athlete. You need to practice on the field, learn the techniques and learn the plays, but you also need to put in your time in the weight room to develop the strength, speed and stamina that make you a better player.

Up to this point we have been working in a nice graphical environment and building a fun game. That's like being on the field. Now that we have done this and have a basic understanding of the game, we are going to go back into the _digital weight room_ and build up some core programming strength.

### Math Again...

As we've seen while developing our game, we sometimes have to do some math to make our code work. The problem we will be tackling today is all about math.

#### Prime Numbers

A prime number is a positive integer that is not evenly divisible by any other integer but 1 and itself. For example, 5 is prime because the only integers we can evenly divide it by, without a remainder, are 1 and 5. On the other hand, 6 is not prime (we call numbers that are not prime _composite_) because we can divide it by 2 and get 3. The number 1 is not considered a prime. So the first few prime numbers are:

    2, 3, 5, 7, 11, 13, 17, 19, ...

#### The Sieve of Eratosthenes

The Sieve of Eratosthenes is a way of finding prime numbers. It can be written as a computer program, but the technique was invented in ancient Greece, long before computers existed.

The sieve is more efficient than most other methods for finding prime numbers because it doesn't actually check whether an individual number is prime. Instead, it starts with the numbers 2 through _n_ where _n_ is the largest number it is interested in checking, and then eliminates all composite numbers up to a certain maximum value. Then, all that is left are the primes.

The way the sieve eliminates composite numbers is by first eliminating all multiples of 2. These numbers are obviously not prime since we can divide them by 2. Then, it removes all multiples of 3, which must also be composite. It continues until it has eliminated multiples of all multiples of numbers up to _n_. What is left are the prime numbers.

### From Math to Code

#### The Simplest Sieve

Here is a very simple version of the sieve code in python.

```python
def sieve(n: int):
    """
    Simple sieve of Eratosthenes to find prime numbers.
    :param n: Look for primes up to and including this number.
    :return: A list of primes less than n.
    """

    # numbers is a list of all the numbers between
    # 2 and n - 1 that might be prime, that is the numbers
    # we have not determined are composite yet.
    numbers = list(range(2,n))

    # Look at each ii that we want to eliminate multiples of:
    for ii in range(2,n):
        # Eliminate the multiples of ii
        for composite in range(2*ii, n+1, ii):
            # If we have not already removed it, remove it now.
            if composite in numbers:
                numbers.remove(composite)

    # Now all that is left are the primes
    return numbers
```

You can invoke this code from a main function at the bottom of your python file like this:

```python
def main():
    n = 20

	# Generate all the primes.
    primes = sieve(n)

	# Print a message. Note that we use format
	# to replace the {} in the string with the
	# value of n. So if we change n above, it will
	# always print properly.
    print("The primes that are less than {} are:".format(n))

	# Now print each of the primes.
    for prime in primes:
        print(prime)

# This is the standard Python idiom to run
# a main function if this file is run directly
# from either the IDE or the command line.
if __name__ == "__main__":
    main()
```

If you type both of these code fragments into a file called `primes.py` and then run it, you should see output that looks like 

```
The primes between 2 and 20 are:
2
3
5
7
11
13
17
19
```

### Optimizing the Sieve

When we write code, we would like it to be as efficient as possible. Computers are fast, but if we can avoid asking them to
do lots of extra work when they don't have to, we can make our programs run faster of do more in the same amount of time.

We don't necessarily want to over-optimize our programs, by doing really obscure things that make the code harder to read, but we do want to do the simple things that make a big difference.

### Optimizing with Data Structure Choices

Often, the best way to optimize code is by choosing the best data structure. A data structure is an object or objects that hold data. For example, a list is a data structure. 

Often a particular data structure will be good (meaning fast) at one kind of operation, but not so good (meaning slow) at others. The only real data structure we are using at this point is a list, so let's think about what it is good and bad at.

Let's think about operations we can do on a list. We can get or set an element of a list with the square brackets. For example

```
a[2] = 17
```

or

```
x = a[12]
```

The square bracket operation, also called indexing, is something computer are very good at. Their processors and memory systems are carefully designed to be able to do this sort of operation really quickly, because it is really common.

Now let's think about the other operations we are doing on the list. One thing we do is ask the question

```
if composite in numbers:
```

How long does this take? Well it turns out it could take much longer than the square brackets because we might have to do the equivalent of square bracket indexing many many times. In fact, we might have to check every element in the array. After all, we don't know for sure it isn't there until we have checked them all. So if we had to do what `in` does, our code would look something like this:

```
def is_in(value, array):
	for ii in range(len(array)):
		if value == array[ii]:
			return True
	
	return False		
``` 

Notice that we have to check a lot of elements of `array` with our square bracket operator, maybe even all of them. This is much more expensive that just a single use of the square brackets.

Now what about the other operation we do, removing an element from the array. We wrote this as

```
numbers.remove(composite)
```

above. How can we remove a value from an array? Again we have to look everywhere, because we don't know where it might be. And worse than that, when we remove it, we have to move all the elements after it forward to fill the space we left. So this is a very expensive operation.

So one way to optimize our code would be to get rid of those expensive operations and replace them with simpler, faster operations like square brackets. One way we can do that is by leaving the `numbers` array alone and using a second array, `is_prime` to hold a `True` or `False` value to indicate whether the index is prime. We can do this with a new `sieve1` function that looks like this

```
def sieve1(n: int):
    """
    Faster sieve of Eratosthenes to find prime numbers.
    :param n: Look for primes up to and including this number.
    :return: A list of primes less than n.
    """

    # is_prime is an array of booleans (True or False)
    # indicating whether numbers are prime. If x is prime
    # then we want to end up with is_prime[x] == True,
    # and if it is composite, we want is_prime[x] == False.

    # Initialize the array to have n elements, all True.
    is_prime = [True] * n

    # 0 and 1 are composite
    is_prime[0] = False
    is_prime[1] = False

    # Look at each ii that we want to eliminate multiples of:
    for ii in range(2,n):
        # Eliminate the multiples of ii
        for composite in range(2*ii, n, ii):
            is_prime[composite] = False

    # Now construct a list of primes as a list of
    # all the ii's less than n which we did not
    # mark as composite.
    primes = [ii for ii in range(n) if is_prime[ii]]

    return primes
```

It looks a lot like the function we had before, but it has a few key differences. First, we set up the `is_prime` array to contain `n` values, all of which are `True`. The syntax

```
[True] * n
```

is just a shorthand way of doing this. We could have equivalently written:

```
is_prime = []
for ii in range(n):
	is_prime.append(True)
```

Now, inside the two loops where we find composite numbers, instead of looking for the composite number in an array and then removing it, we simply say

```
is_prime[composite] = False
```

That's one square bracket operation instead of a bunch of them.

Finally, after all the composites have been eliminated, we need to get a list of the the primes that are left. We could do this with code like:

```
primes = []
for ii in range(n):
	if is_prime[ii]:
		primes.append(ii)
```

Instead, we chose a more compact syntax

```
primes = [ii for ii in range(n) if is_prime[ii]]
```

that does essentially the same thing.

Finally, we just return the list.

#### Does it Make a Difference?

If we want to see if our changes made a difference, we can time how long it takes for them to run for a large `n`. First, we need some code from the `time` package. So at the top of your file add the line

```
import time
```

Now, change your `main()` to time both versions of the code, as follows:

```
def main():
    # Choose a large n
    n = 10000

	 # Time the original version of the code:
    start0 = time.clock()
    primes = sieve(n)
    end0 = time.clock()

    elapsed0 = end0 - start0

	 # Timo the improved version of the code. 
    start1 = time.clock()
    primes1 = sieve1(n)
    end1 = time.clock()

    elapsed1 = end1 - start1

	 # Print our results.
    print("sieve0 found {} primes less than {} in {}".format(len(primes), n, elapsed0))
    print("sieve1 found {} primes less than {} in {}".format(len(primes1), n, elapsed1))
    print("Speedup: {}".format(elapsed0 / elapsed1))
```

When I run this code on my laptop, the results are

```
sieve0 found 1229 primes less than 10000 in 2.3831420000000003
sieve1 found 1229 primes less than 10000 in 0.010460999999999832
Speedup: 227.8120638562316
```

So the improved version is over 200 times faster than the original. Try this with your code for different values of `n` on your computer and see how it comes out.

We've just scratched the surface of optimization, and as we mentioned earlier, there are certainly cases where it's not worth the effort. If you have to make major changes to your code that make it hard for others to read and understand, and you only get a 5% speedup, you might not want to bother. But if a few simple changes like the ones we made here can make your code 200 times faster, it's probably worth the effort.

As you spend more time in the weight room, you'll develop intuition around what operations are fast and what operations are slow, and you will tend to write faster code to begin with. This is just like building up your strength in the gym so you can perform better on the field.
