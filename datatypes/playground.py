

# Create some arrays:

a = []
b = [ 10, 20, 30 ]

print(a)
print(b)

# How big are the arrays?

print(len(a))
print(len(b))


# Get an element out of an array:

x = b[0]
y = b[1]

print("x:", x)
print("y:", y)

# Does an array contain a given element?

c20 = 20 in b
c50 = 50 in b

print("c20:", c20)
print("c50:", c50)

# Add an element to an array:

print("a was:", a)
a.append(500)
print("a is :", a)

for ii in range(0, 10000, 1000):
    a.append(ii)

l = len(a)

print("Now, a has", l, "elements.")
print("a:", a)

print()

# We can iterate through an array:

for x in a:
    print(x)

print()

print("a:", a)
print("b:", b)
c = a

for x in b:
    c.append(x)

print("c:", c)




# Arrays can have more than numbers in them.

ar1 = [ "An array can contain...", "strings" ]
print("ar1:", ar1)

ar2 = [ "An array can contain...", 17, "a mix of things."]

for thing in ar2:
    print(thing)

ar3 = [ "An array can contain...", [ 10, 20, 30], "another array!" ]
print("ar3:", ar3)

# We can even put arrays inside arrays inside arrays...

aa = [ 1, [ 2, [ 132, [ "Hi!", [ ar3, [  ]]]]]]

print("aa", aa)

# What else can we do with arrays? We can sort them.

a10 = [ 17, 51, 3, 97, 13 ]
a10s = sorted(a10)

print("Sorted: ", a10s)

# We can also sort in reverse:

a10r = sorted(a10, reverse=True)

print("Reversed: ", a10r)

# What about numbers and strings?

#ans = sorted([10, 5, 'hello', 'abc', -2])

#ana = sorted([10, 5, ['hello', 'abc'], -2])


# A dictionary is like an array, but instead of an integer for
# an index, we use a string.

# For example, we could use a dictionary to keep track
# of how many of each item we have in a store:
inventory = { "widgets": 10, "gadgets": 15 }

print("Inventory:", inventory)

print()

w = inventory['widgets']
g = inventory['gadgets']

print("We have ", w, "widgets and", g, "gadgets")

print()

# We can add items to the dictionary

inventory['toys'] = 12
inventory['games'] = 5

print("Inventory:", inventory)

print()

#

names = [ 'apple', 'banana', 'grape', 'mango', 'pizza']

places = {}

for ii in range(len(names)):
    places[names[ii]] = ii

print("Places: ", places)

print()


# What if we don't know what all the keys are?
# We can ask the dictionary.

k = inventory.keys()

print("Inventory keys:", k)

# More commonly, we want to do something with each:
for k in inventory:
    print(k)

print()

# And we want the value, not just the key:
for k in inventory:
    print("We have:", inventory[k], k)

print()

# Or maybe we just want to know what we are low on:

for k in inventory:
    if inventory[k] < 10:
        print("We are low on", k)

print()

# Perhaps we have two dictionaries, one with predicted demand
# and one with inventory:

demand = { "widgets": 20, "gadgets": 10, "toys": 7, "games": 20 }

for k in demand:
    if inventory[k] < demand[k]:
        print("We don't have enough", k)

print()

for k in demand:
    if inventory[k] >= demand[k]:
        print("We have enough", k)

print()

demand['apples'] = 100

for k in sorted(demand):
    if k in inventory:
        if inventory[k] < demand[k]:
            print("We don't have enough", k)
        else:
            print("We have enough", k)
    else:
        print("We don't have any", k)

# Write your own version of this that tells us how many
# of each item we need to order, i.e. demand[k] - inventory[k]
# if we don't have enough inventory or how many extra
# we will have, i.e. inventory[k] - demand[k] if we do
# have enough.

print()

# Let's go back to arrays. We can filter an array or
# arraylike object as follows:


an = [ 2, 17, 8, 4, 92, 14, 3 ]
af = [ x for x in an if x < 10 ]
print("af:", af)


missing = [item for item in demand if item not in inventory]
enough  = [item for item in demand if item in inventory and inventory[item] >= demand[item]]
insufficient = [item for item in demand if item in inventory and inventory[item] < demand[item]]

print("Missing     :", sorted(missing))
print("Insufficient:", sorted(insufficient))
print("Enough      :", sorted(enough))

print()



# Now let's add prices

prices = {
    "apples": 0.49,
    "games" : 5.99,
    "widgets" : 1.79,
    "gadgets" : 7.99,
    "toys" : 1.29
}

# If we sell exactly the demand, how much revenue will we have?

demand_revenue = 0.00

for item in demand:
    demand_revenue = demand_revenue + demand[item] * prices[item]

print("Demand revenue    : {:.2f}".format(demand_revenue))

# What if we only sell the inventory we have, even if demand is higher?

max_revenue = 0

for item in demand:
    can_sell = 0
    if item in inventory:
        if demand[item] <= inventory[item]:
            can_sell = demand[item]
        else:
            can_sell = inventory[item]

    max_revenue = max_revenue + can_sell * prices[item]

print("Max revenue       : {:.2f}".format(max_revenue))
print("---------------------------")
print("Missed Opportunity: {:.2f}".format(demand_revenue - max_revenue))


# Holiday demand multiplier:

demand_multiplier = { "apples": 2, "toys" : 4, "games": 3 }

# Make a holiday demand dictionary by multiplying
# the demand multiplier by each item type's demand
# or just use 1 as the multiplier if there is no
# multiplier given.





