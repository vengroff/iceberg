import arcade

import arcade.color as color

class lifeboard(arcade.Window):

    class cell(object):
        def __init__(self,x,y,color = color.WHITE):
            self.x = x
            self.y = y
            self.color = color

        def __str__(self):
            return "cell({},{}): {}".format(self.x, self.y, self.color)

    def __init__(self, width=16, height=9, cellpels=50):
        pelwidth = width * cellpels
        pelheight = height * cellpels
        super().__init__(pelwidth, pelheight, title="Life Board")

        self.cellpels = cellpels

        self.board = [];

        for x in range(width):
            col = [lifeboard.cell(x,y) for y in range(height)]
            self.board.append(col)

        #arcade.open_window("Life Board", pelwidth, pelheight)
        #arcade.set_background_color(arcade.color.WHITE)
        #arcade.schedule(lambda delta_time: self.draw(delta_time), 1/30.0)

    def animate(self, delta_time: float):
        pass

    def run(self):
        arcade.run()

    def setColor(self, x, y, color):
        self.board[x][y] = lifeboard.cell(x,y,color)

    def on_draw(self):

        arcade.start_render()

        for col in self.board:
            for cell in col:
                pelx = (cell.x + 0.5) * self.cellpels
                pely = (cell.y + 0.5) * self.cellpels
                arcade.draw_rectangle_filled(pelx, pely, self.cellpels, self.cellpels, color.WHITE)
                arcade.draw_rectangle_filled(pelx, pely, self.cellpels - 2, self.cellpels - 2, cell.color)
