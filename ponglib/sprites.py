"""
TODO -

1. Handle keystrokes to change the velocity of the apple.
2. Add vertical gravity to the sprites.
3. Make the balls want to follow the apple.
"""


import arcade
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

<<<<<<< HEAD
MAX_SPEED = 8
=======
MAX_SPEED = 5
>>>>>>> dev/guess-numbers

WINNING_SCORE = 10

BLINK_FRAMES = 15

# States
(PROMPT, PLAYING, GAME_OVER) = range(3)


class MovingSprite(arcade.Sprite):

    def __init__(self, filename: str, bounce: bool):
        super().__init__(filename)
        self.center_x = SCREEN_WIDTH / 2
        self.center_y = SCREEN_HEIGHT / 2
        self.x_velocity = 0
        self.y_velocity = 0
        self.bounce = bounce

    def update(self):
        self.center_x = self.center_x + self.x_velocity
        self.center_y = self.center_y + self.y_velocity

        if self.bounce:
            # Bounce
            #if self.left < 0 or self.right > SCREEN_WIDTH:
            #    self.x_velocity = -self.x_velocity
            if self.bottom < 0 or self.top > SCREEN_HEIGHT:
                self.y_velocity = -self.y_velocity
        else:
            if self.bottom < 0 or self.top > SCREEN_HEIGHT:
                self.y_velocity = 0


class PaddleSprite(MovingSprite):
    """
    Apples are under user control.
    """
    def __init__(self,x,filename):
        super().__init__(filename,False)
        self.center_x = x

class BallSprite(MovingSprite):
    """
    balls move around on their own.
    """
    def __init__(self):
        super().__init__('images/ball70.png',True)
        # random.random() returns a number x >= 0 and < 1.
        # We will use it as a fraction of max speed.
        self.x_velocity = random.choice([-1,1]) * MAX_SPEED
        self.y_velocity = random.uniform(-1,1) * MAX_SPEED
        self.width = 70
        self.height = 70

    def update(self):
        super().update()
        #self.angle = self.angle + 340
        #if self.angle > 360:
        #    self.angle = self.angle % 360

class PongBoard(arcade.Window):
    """
    A board that contains sprites, which are objects that
    move around step by step.
    """

    def __init__(self):
        """
        Set up the board.
        """

        # Initialize the Window by doing whatever needs
        # to be done by our super class.
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, title="Pong")

        # Initially, we have no balls at all.
        self.balls = arcade.SpriteList()
        arcade.set_background_color(arcade.color.WHITE)

        # Add the paddles
        self.paddle1 = PaddleSprite(15,'images/purplepaddle.png')
        self.paddle2 = PaddleSprite(SCREEN_WIDTH - 15,'images/redpaddle.png')

        self.reset_game()

    def reset_game(self):
        # Initial score.
        self.score1 = 0
        self.score2 = 0

        self.state = PROMPT

        self.winner = 0
        self.win_frames = 0


    def paddle_bounce(self):
        # Detect collisions.
        for ball in self.balls:
            # Collision with left paddle?
            if (ball.left > 0 and ball.left <= self.paddle1.right and
                ball.center_y <= self.paddle1.top and
                ball.center_y >= self.paddle1.bottom):
                ball.x_velocity = abs(ball.x_velocity)
                pos = (ball.center_y - self.paddle1.center_y) / (self.paddle1.top - self.paddle1.center_y)
                ball.y_velocity = pos * MAX_SPEED
            # Collision with right paddle?
            if (ball.right < SCREEN_WIDTH and ball.right >= self.paddle2.left and
                ball.center_y <= self.paddle2.top and
                ball.center_y >= self.paddle2.bottom):
                ball.x_velocity = -abs(ball.x_velocity)
                pos = (ball.center_y - self.paddle2.center_y) / (self.paddle2.top - self.paddle2.center_y)
                ball.y_velocity = pos * MAX_SPEED

    def detect_scoring(self):
        balls_in_play = arcade.SpriteList()

        for ball in self.balls:
            if ball.right <= 0:
                self.score2 = self.score2 + 1
                if self.score2 >= WINNING_SCORE:
                    self.winner = 2
                    self.state = GAME_OVER
                else:
                    self.state = PROMPT
            elif ball.left >= SCREEN_WIDTH:
                self.score1 = self.score1 + 1
                if self.score1 >= WINNING_SCORE:
                    self.winner = 1
                    self.state = GAME_OVER
                else:
                    self.state = PROMPT
            else:
                balls_in_play.append(ball)

        self.balls = balls_in_play;



    def animate(self, delta_time: float):
        """
        Animate all the sprites we have on the board.
        """

        if self.state == PLAYING:
            self.balls.update()

        # Let them still move paddles even if
        # there is a winner.
        self.paddle1.update()
        self.paddle2.update()

        if self.state == PLAYING:
            # Bounce off paddles.
            self.paddle_bounce()

            # Detect scoring.
            self.detect_scoring()

    def on_draw(self):
        """
        Draw the sprites.
        """
        arcade.start_render()

        self.balls.draw()
        self.paddle1.draw()
        self.paddle2.draw()

        # Draw the score.
        arcade.draw_text(str(self.score1), 10, SCREEN_HEIGHT - 100, arcade.color.DARK_GRAY, 72, 200, "center", bold=True)
        arcade.draw_text(str(self.score2), SCREEN_WIDTH - 210, SCREEN_HEIGHT - 100, arcade.color.DARK_GRAY, 72, 200, "center", bold=True)

        if self.state == PROMPT:
            arcade.draw_text('Hit spacebar to serve the ball.', 0, SCREEN_HEIGHT / 2, arcade.color.RED, 36, SCREEN_WIDTH, "center", bold=True)
        elif self.state == GAME_OVER:
            if self.win_frames < BLINK_FRAMES:
                color = arcade.color.RED
                winner = 'Player ' + str(self.winner) + ' wins!'
                arcade.draw_text(winner, 0, SCREEN_HEIGHT / 2, color, 60, SCREEN_WIDTH, "center", bold=True)

            arcade.draw_text('Hit spacebar to start a new game.', 0, SCREEN_HEIGHT / 2 - 100, arcade.color.RED, 36, SCREEN_WIDTH, "center", bold=True)

            self.win_frames = (self.win_frames + 1) % (2 * BLINK_FRAMES)

    def on_key_release(self, symbol: int, modifiers: int):
        if symbol == ord('q') or symbol == ord('z'):
            self.paddle1.y_velocity = 0
        if symbol == arcade.key.UP or symbol == arcade.key.DOWN:
            self.paddle2.y_velocity = 0

    def on_key_press(self, symbol: int, modifiers: int):
        """
        This function is called when a key on the keyboard is pressed.
        :param symbol: the key that was pressed
        :param modifiers: modifiers on the key, like control.
        :return: void
        """

        if self.state == PROMPT:
            if symbol == ord(' '):
                # Add a ball
                if len(self.balls) < 20:
                    self.balls.append(BallSprite())
                self.state = PLAYING
        elif self.state == GAME_OVER:
            if symbol == ord(' '):
                self.reset_game()

        if symbol == ord('q'):
            if self.paddle1.top < SCREEN_HEIGHT:
                self.paddle1.y_velocity = 10
            else:
                self.paddle1.y_velocity = 0
        elif symbol == ord('z'):
            if self.paddle1.bottom > 0:
                self.paddle1.y_velocity = -10
            else:
                self.paddle1.y_velocity = 0
        elif symbol == arcade.key.UP:
            if self.paddle2.top < SCREEN_HEIGHT:
                # Paddle 2 up.
                self.paddle2.y_velocity = 10
            else:
                self.paddle2.y_velocity = 0
        elif symbol == arcade.key.DOWN:
            if self.paddle2.bottom > 0:
                # Paddle 2 down
                self.paddle2.y_velocity = -10
            else:
                self.paddle2.y_velocity = 0
