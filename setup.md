# Setup for Iceberg

## Windows

### Git for Windows

URL: [https://git-for-windows.github.io/]()

- Download and install. 
- Take defaults for everything except add a check to the box to put an icon on the desktop.


### Python 3.5
URL: [https://www.python.org/downloads/release/python-353/]()

- Download Windows x86-64 executable installer and install. 
- Click the add to path checkbox on the initial install page. 
- Click custom installation. Then click next. 
- Select install for all users. This will include IDLE.
	
### pyCharm CE
URL: [https://www.jetbrains.com/pycharm/download/#section=windows]() 

- Download community edition.
- Check boxes for .py association and 32-bit desktop launcher. 
- Defaults on the next page.

### Arcade

- Right click the windows icon and open “Command Prompt (Admin). Type the command: 

```
    pip3 install arcade 
```
### iceberg on gitlab

URL: [https://gitlab.com/vengroff/iceberg]()

- Copy the git url from the page above (https://gitlab.com/vengroff/iceberg.git). 
- In git bash: 

```
    git clone https://gitlab.com/vengroff/iceberg.git
```




