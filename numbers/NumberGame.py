import arcade
import random

WIDTH = 800
HEIGHT = 600

class NumberWindow(arcade.Window):
    (START,GUESS,HIGH_LOW,WIN,GAME_OVER) = range(5)

    def __init__(self):
        super().__init__(WIDTH,HEIGHT)
        arcade.set_background_color(arcade.color.WHITE)
        self.reset()

    def reset(self):
        self.number = random.randint(0,999)
        self.turnsLeft = (10)
        self.currentGuess = None
        self.state = NumberWindow.START
        print(self.number)

    def on_draw(self):
        arcade.start_render()
        if self.state == NumberWindow.START:
            arcade.draw_text("Welcome to the NUM83R G4M3", WIDTH / 8, 2 * HEIGHT / 3, arcade.color.ROYAL_PURPLE, 24,
                             3*WIDTH/4, "center")
            arcade.draw_text("Press ENTER to Begin", WIDTH / 8, 7 * HEIGHT / 12, arcade.color.ROYAL_PURPLE, 24,
                             3 * WIDTH / 4, "center")
        elif self.state == NumberWindow.GUESS:
            arcade.draw_text("{} turns left".format(self.turnsLeft),
                             WIDTH / 8, 2 * HEIGHT / 12, arcade.color.ROYAL_PURPLE, 24, 3 * WIDTH / 4, "center")
            arcade.draw_text("Enter a number between 0 and 999 Then press ENTER key to check it.",
                             WIDTH / 8, 5*HEIGHT / 12, arcade.color.BLACK, 24, 3* WIDTH / 4, "center")
            if self.currentGuess is not None:
                arcade.draw_text("{}".format(self.currentGuess),
                                 WIDTH / 8, 9 * HEIGHT / 12, arcade.color.RED, 48, 3 * WIDTH / 4, "center")
        elif self.state == NumberWindow.WIN:
            arcade.draw_text("CONGRATULATIONS, YOU WIN",
                             WIDTH / 8, 7 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
            arcade.draw_text("You had {} remaining".format(self.turnsLeft),
                             WIDTH / 8, 6 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
            arcade.draw_text("Press ENTER to play again",
                             WIDTH / 8, 5 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
        elif self.state == NumberWindow.GAME_OVER:
            arcade.draw_text("GAME OVER",
                             WIDTH / 8, 9 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
            arcade.draw_text("Number was {}" .format(self.number),
                             WIDTH / 8, 7 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
            arcade.draw_text("Press ENTER to play again",
                             WIDTH / 8, 5 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
        elif self.state == NumberWindow.HIGH_LOW:
            arcade.draw_text("{} turns left".format(self.turnsLeft),
                             WIDTH / 8, 2 * HEIGHT / 12, arcade.color.ROYAL_PURPLE, 24, 3 * WIDTH / 4, "center")
            arcade.draw_text("{}".format(self.currentGuess),
                             WIDTH / 8, 9 * HEIGHT / 12, arcade.color.RED, 48, 3 * WIDTH / 4, "center")
            if self.currentGuess < self.number:
                arcade.draw_text("Your guess was too low.",
                                WIDTH / 8, 5 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
                arcade.draw_text("Press ENTER to try again.",
                                WIDTH / 8, 6 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
            else:
                arcade.draw_text("Your guess was too high. Try again.",
                                 WIDTH / 8, 5 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")
                arcade.draw_text("Press ENTER to try again.",
                                 WIDTH / 8, 6 * HEIGHT / 12, arcade.color.BLACK, 24, 3 * WIDTH / 4, "center")


    def on_key_press(self, symbol: int, modifiers: int):
        if self.state == NumberWindow.START:
            if symbol == arcade.key.ENTER:
                self.state = NumberWindow.GUESS
        elif self.state == NumberWindow.GUESS:
            if symbol >= arcade.key.NUM_0 and symbol <= arcade.key.NUM_9:
                digit = symbol - arcade.key.NUM_0
                if self.currentGuess is None:
                    self.currentGuess = digit
                elif self.currentGuess < 100:
                    self.currentGuess = self.currentGuess*10 + digit
            elif symbol == arcade.key.BACKSPACE:
                if self.currentGuess is not None:
                    if self.currentGuess >= 10:
                        self.currentGuess = int(self.currentGuess / 10)
                    else:
                        self.currentGuess = None
            elif symbol == arcade.key.ENTER:
                if self.currentGuess is not None:
                    if self.currentGuess == self.number:
                        self.state = NumberWindow.WIN
                    else:
                        if self.turnsLeft > 1:
                            self.state = NumberWindow.HIGH_LOW
                            self.turnsLeft -= 1
                        else:
                            self.state = NumberWindow.GAME_OVER
        elif self.state == NumberWindow.WIN:
            if symbol == arcade.key.ENTER:
                self.reset()
        elif self.state == NumberWindow.HIGH_LOW:
            if symbol == arcade.key.ENTER:
                self.state = NumberWindow.GUESS
                self.currentGuess = None
        elif self.state == NumberWindow.GAME_OVER:
            if symbol == arcade.key.ENTER:
                self.reset()


















def main():
    window = NumberWindow()
    arcade.run()


if __name__== '__main__':
    main()


