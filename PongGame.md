## Adding Features to Complete Pong

There are several key places in the code that we can add features:

### Sprite Code:

The `update(self)` method is the key method for moving sprites. This method is used to move a sprite from one location to the next. The places where we might want to
make changes to `update(self)` are:

1. In the `MovingSprite` class if we want the new functionality to apply to all sprites. 
1. In the `PaddleSprite` class if we want the new functionality to apply to just the paddles.  
1. In the `BallSprite` class if we want the new functionality to apply to just the ball.


### PongBoard Code:

In the `PongBoard` class there are several places we might want to make changes:

1. In `__init__(self)`. This is where the board is set up for the first time. We set up the board's size, add the initial sprites, and get ready to play the game. Note that anything we do here happens right when we start up, so we don't necessarily want to create all the sprites here. Some of them may get created when we hit keys.
2. In `on_draw(self)`. This is where we draw the screen. We draw all the sprites, but we might also want to draw things like the score, using something like:

  ```
# Draw the score.
arcade.draw_text(str(self.score1), 10, SCREEN_HEIGHT - 100, arcade.color.DARK_GRAY, 72, 200, "center", bold=True)
arcade.draw_text(str(self.score2), SCREEN_WIDTH - 210, SCREEN_HEIGHT - 100, arcade.color.DARK_GRAY, 72, 200, "center", bold=True)
  ```
This draws the two players' scores, assuming they are stored as `self.score1` and `self.score2` on the `PongBoard`. We would probably want to set these to 0 in `__init__(self)`. Note also that we can use this same `arcade.draw_text(...)` function to draw messages for the user to see, like

  ```
     arcade.draw_text('Hit spacebar to serve the ball.', 0, SCREEN_HEIGHT / 2, arcade.color.RED, 
                      36, SCREEN_WIDTH, "center", bold=True)
  ```

3. In `animate(self, delta_time: float)`. This method is where we do things like update all the sprites and perform collision detection to determine if the ball needs to bounce. This is also where we could detect that a ball has been missed (gone off the screen) and adjust the score accordingly.
4. In `on_key_press(self, symbol: int, modifiers: int)` and `on_key_releasse(self, symbol: int, modifiers: int)` where we will want to take different actions when keys are pressed.

### Adding States

As we move forward with the game we will want to add states to the game. States will let us keep track of whether the game is just beginning, in progress, waiting to serve, or over. We can then look at the state in the board's `draw` method to decide what to draw on the screen.

We can define states before all our classes using code like

```
# States
(GAME_START, WAITING_TO_SERVE, PLAYING, GAME_OVER) = range(4)
```

And then we can set up the initial state with

```
self.state = GAME_START
```

in the `__init__` method of the board.

We will check and change the state as needed in `on_key_press` and `animate`, and we will check the state in `on_draw` to decide what we need to draw.

In the `GAME_START` state we might have some instructions. In the `WAITING_TO_SERVE` state, we might have a message on the screen like

![Waiting to serve screenshot](screenshots/wait_to_serve.png)

and other appropriate messages will be present in the other states.
