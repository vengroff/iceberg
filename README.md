# Welcome to The Iceberg Project

Iceberg is a curriculum designed to teach the fundamentals of software engineering with Python. It is designed to be presented by an instructor, but can also be used for self-study. No prior programming experience in any language is required.

## The Iceberg and the Bridge

Our approach to teaching software engineering is based on two metaphors, the iceberg and the bridge. These metaphors inform how programming concepts are introduced and how lessons are structured.

### The Iceberg

Many of the software systems we use daily, whether we are programmers or not, can be understood by thinking about icebergs. Icebergs have two parts, the part above the water that can be seen, and the part underwater that can't be seen but provides the critical foundation to keep the visible part above the water.

The part of software you interact with, also known as the front end, user interface or UI, is like the visible part of the iceberg. It consists of all the screens you see when you run a program, the buttons you click on, the boxes you type in, the animations, audio and video that you watch, and nowadays the voice commands you give.

But just like an iceberg, most software has a large invisible substructure that is absolutely critical to support the user interface. These components are sometimes called the back end. The back end includes all the code and data storage an application relies on to do it's work. Sometimes the back end code runs on the same computer or mobile device as the front end, and sometimes much of it runs on completely separate servers, often referred to as the cloud. Back end code also includes methods for coordinating activities across several different computers to get a single overall task done. This is called distributed computing.

Our approach is to begin at the software equivalent of the waterline of the iceberg. This means we will alternate between writing a little bit of front end code and a little bit of back end code. We will build a really small iceberg, and then as we alternate back and forth from front end to back end, we will build bigger and bigger icebergs that do more and more. This approach is sometimes called full-stack software engineering.

Sometimes, front end and back end code are written in different languages. Since we are just starting out, we will deal with just one language, Python, for both. 

### The Bridge

Imagine you are standing on one side of a deep and wide ravine. There is no bridge to get to the other side, but you would like to build one. Building a bridge is a different kind of engineering than software engineering, but it has some useful analogies.

There are a lot of different kinds of bridges we could build. Some simple bridges are only be able to support a single person walking across at a time. A better bridge could let many people walk across at once, or ride bicycles or even motorbikes across. Other more advanced types of bridges can support cars, trucks and trains or all three simultaneously.

The more we want our bride to support, the more complicated and carefully designed it needs to be in order to do it's job. This means it will take more time and money than a smaller simpler bridge.

The good news is that we can often build a simple bridge and make it more advanced over time. This is even more true in software than it is in bridges.

Let's go back to the thought experiment where you are standing on one side of the ravine. Now imagine you have a bow and arrow and a long string. One end of the string is tied to a tree and the other end is tied to the back of the arrow. If you shoot the arrow across the ravine so it sticks into a tree on the other side, you now have a very simple bridge. It's made of string, it can't hold a very heavy load, but it can be used to carry small loads across the ravine.

Now imagine your friend on the other side of the ravine unties the string from the arrow and ties it to the end of a long piece of rope. Now you can pull in the string until the rope crosses the ravine. You and your friend can each tie the rope to a tree on your side of the ravine. Now you have a better bridge.

Next you could use the rope to pull a bigger rope across, or you could hang a wooden box from a pulley on the rope. Now you can use the box to transport goods across the ravine. If the box is big enough you could even transport a person.

At this point, the bridge is useful enough that you or other people can use it for something other than just constructing a bigger bridge. In the software world, we call this a Minimum Viable Product, or MVP. It is the smallest, simplest to build version of a product that is sufficiently useful that people would really want to use it.

Eventually, we could add all kinds of features to the bridge. We could use multiple ropes. We could use steel cable instead of rope. We could add wooden planks to make a walkway. If we wanted to support heavier loads we could build towers out of steel and concrete instead of using trees as anchors. Then instead of a walkway we could suspend a roadway from the cables.

Adding all of these additional features to the bridge could take years and cost millions of dollars. But by using the MVP approach, we can build a useful bridge in hours or days. From there we iterate to make the bridge better and better, but we always have a working bridge.

This is how we will approach learning software engineering. We will begin by building programs that are simple, but accomplish real goals. We will then extend them, learn more advanced engineering practices, and extend and improve them again. But we will never be in a state where we don't have something that works.

# Getting Started

## Attack of the Clones

We talked about building a bridge starting with string and rope. But we didn't talk about growing cotton or hemp to get the fibers that went into the string and rope, much less the process of spinning them. We assumed someone else had already done that and we could get the string and rope from them.

It works the same way in software. We don't generally start with a completely empty slate. Those who came before us have created not only string and rope, but tools like scissors to cut them. They have also tried all kinds of different ways to tie knots and written down instructions for how to tie the best knots for a wide variety of uses. We would be silly not to take advantage of these tools and materials.

One of the most common ways to obtain tools and software components you need to help build your MVP is through something called a source code repository. There are a number of source code repositories on the internet. Github and Gitlab are two that are particularly well known. Each of them has software tools and components submitted by thousands of programmers and available, often for free, to millions of other programmers.

The process of downloading a copy of code from a source code repository to your own computer is called cloning. Cloning makes a copy of not only the current code, but all the changes that led to it's current state, often accompanied by comments from the original programmers explaining what they did and why.

To get started, you can clone this repository from the URL [https://gitlab.com/vengroff/iceberg](https://gitlab.com/vengroff/iceberg). The command-line way to do this is 

`git checkout https://gitlab.com/vengroff/iceberg`

You can also check it out with a GUI git tool by copying and pasting the URL above into your GUI tool.

When you do this, you will get a local copy of this file, other documentation, and all the supporting files for the various lessons in the project.

 

 


