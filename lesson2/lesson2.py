"""
Lesson 2 - Inversion of Control

This lesson is all about building programs that react to
events. Instead of just executing code we write in a straight
line like we have done in the past, we will instead write code
that reacts to events from the outside world, like a user hitting
a key or an animation system wanting to draw a new frame of the
animation.
"""

import arcade

class BouncingBall(arcade.Window):
    """
    BouncingBall is a subcless of arcade.Window. This means it does
    everything a Window does, but it does a few things in
    different special ways as we define below.
    """
    def __init__(self, width=800, height=600):
        """
        This is the function that is called to initialize
        a brand new object. So if we write

            BouncingBall = BouncingBall()

        or

            BouncingBall = BouncingBall(1000, 1000)

        this is the function that is called to set it up.
        If we don't pass in width or height, default values
        are used.
        """

        # Initialize the Window by doing whatever needs
        # to be done by our super class.
        super().__init__(width, height, title="BouncingBall")

        # Now do our own special setup.
        self.current_x = width / 2
        self.current_y = height / 2
        arcade.set_background_color(arcade.color.BLACK)

    def draw_ball(self, x, y):
        arcade.draw_rectangle_filled(x, y, 80, 80, arcade.color.RED)
        arcade.draw_rectangle_outline(x, y, 80, 80, arcade.color.WHITE, 10)

    def on_draw(self):
        """
        Draw the ball on the screen at it's current location.
        When we start the arcade system by calling arcade.run(),
        arcade will start calling this repeatedly, as often as it
        can to try to maintain a good animation frame rate.

        Notice that we never call this function ourselves. We rely
        on arcade to call it over and over again every time it
        thinks it needs to display a new frame.
        """
        arcade.start_render()
        self.draw_ball(self.current_x, self.current_y)
        if self.current_x < 40:
            self.draw_ball(self.width+self.current_x, self.current_y)
            if self.current_y < 40:
                self.draw_ball(self.width+self.current_x, self.height + self.current_y)
            elif self.current_y > self.height - 40:
                self.draw_ball(self.width+self.current_x, self.current_y - self.height)
        elif self.current_x >self.width-40:
            self.draw_ball(self.current_x - self.width, self.current_y)
            if self.current_y < 40:
                self.draw_ball(self.current_x - self.width, self.height + self.current_y)
            elif self.current_y > self.height - 40:
                self.draw_ball(self.current_x - self.width, self.current_y - self.height)
        if self.current_y < 40:
            self.draw_ball(self.current_x,self.height + self.current_y)
        elif self.current_y > self.height - 40:
            self.draw_ball( self.current_x,self.current_y - self.height)

    def on_key_press(self, symbol: int, modifiers: int):
        """
        This function is called when a key on the keyboard is pressed.
        :param symbol: the key that was pressed
        :param modifiers: modifiers on the key, like control.
        :return: void

        Again, notice that like on_draw(self), we never write code that
        calls this function. Instead, we rely on arcade to see events like
        key presses from the operating system and then pass them on to
        us through this function.
        """

        # TODO: Here are some next steps for you to try:
        #
        # 1. Handle arcade.key.UP and arcade.key.DOWN to change the y
        #    coordinates of the ball.
        #
        # 2. Make the board wrap around. In other words, if the user moves
        #    the ball off the right side of the screen, make it reappear
        #    on the left side of the screen. Same with top and bottom.
        #
        # 3. Make step 2 look even better. If the ball is partially off the
        #    side, draw it twice so that part of the ball is seen leaving
        #    the screen and part is seen reappeating on the opposite side.
        #


        # Notice that we use the if / elif syntax here. This
        # is common when we want to check for multiple possible
        # values a variable might have. If there are more than
        # two possibilities we can add as many elif clauses as we
        # need, so the code looks something like
        #
        #     if x == 1:
        #        # do something
        #     elif x == 2:
        #        # do something else
        #     elif x == 3:
        #        # do some other thing
        #
        # etc... with as many more elif's as you need.

        if symbol == arcade.key.LEFT:
            self.current_x = self.current_x - 20
            if self.current_x < 0:
                self.current_x = self.width
        elif symbol == arcade.key.RIGHT:
            self.current_x = self.current_x + 20
            if self.current_x > self.width:
                self.current_x = 0
        elif symbol == arcade.key.DOWN:
            self.current_y = self.current_y - 20
            if self.current_y < 0:
                self.current_y = self.height
        elif symbol == arcade.key.UP:
            self.current_y = self.current_y + 20
            if self.current_y > self.height:
                self.current_y = 0







def main():
    """
    This is the main function. This is where we want our
    program to start.

    All we really do here is create our window and then tell
    arcade to take over from here, passing us events whenever
    it needs to. We then process those events using the functions
    we wrote in the BouncingBall class.
    :return: void
    """
    ball = BouncingBall()
    arcade.run()


# Run the main function.
main()