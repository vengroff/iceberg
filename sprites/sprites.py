"""
TODO -

1. Handle keystrokes to change the velocity of the apple.
2. Add vertical gravity to the sprites.
3. Make the bananas want to follow the apple.
"""


import arcade
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MAX_SPEED = 5

class MovingSprite(arcade.Sprite):

    def __init__(self, filename: str):
        super().__init__(filename)
        self.center_x = SCREEN_WIDTH / 2
        self.center_y = SCREEN_HEIGHT / 2
        self.x_velocity = 0
        self.y_velocity = 0

    def update(self):
        self.center_x = self.center_x + self.x_velocity
        self.center_y = self.center_y + self.y_velocity

        # Bounce
        if self.center_x < 0 or self.center_x > SCREEN_WIDTH:
            self.x_velocity = -self.x_velocity
        if self.center_y < 0 or self.center_y > SCREEN_HEIGHT:
            self.y_velocity = -self.y_velocity


class AppleSprite(MovingSprite):
    """
    Apples are under user control.
    """
    def __init__(self):
        super().__init__('images/apple.png')

class BananaSprite(MovingSprite):
    """
    Bananas move around on their own.
    """
    def __init__(self):
        super().__init__('images/banana.png')
        # random.random() returns a number x >= 0 and < 1.
        # We will use it as a fraction of max speed.
        self.x_velocity = random.uniform(-1,1) * MAX_SPEED
        self.y_velocity = random.uniform(-1,1) * MAX_SPEED


class SpriteBoard(arcade.Window):
    """
    A board that contains sprites, which are objects that
    move around step by step.
    """

    def __init__(self):
        """
        Set up the board.
        """

        # Initialize the Window by doing whatever needs
        # to be done by our super class.
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, title="Sprite Board")

        # Initially, we have no sprites at all.
        self.bananas = arcade.SpriteList()
        arcade.set_background_color(arcade.color.WHITE)

        # And the apple we control
        self.apple = AppleSprite()

    def animate(self, delta_time: float):
        """
        Animate all the sprites we have on the board.
        """
        self.bananas.update()
        self.apple.update()


    def on_draw(self):
        """
        Draw the sprites.
        """
        arcade.start_render()

        self.bananas.draw()
        self.apple.draw()

    def on_key_press(self, symbol: int, modifiers: int):
        """
        This function is called when a key on the keyboard is pressed.
        :param symbol: the key that was pressed
        :param modifiers: modifiers on the key, like control.
        :return: void
        """

        if symbol == ord('b'):
            if modifiers & arcade.key.MOD_SHIFT:
                # Drop a banana
                if self.bananas:
                    self.bananas.pop()
            else:
                # Add a banana
                if len(self.bananas) < 20:
                    self.bananas.append(BananaSprite())
        # TODO - for the arrow keys, adjust the velocity of the apple.
        elif symbol == arcade.key.LEFT:
            pass
        elif symbol == arcade.key.RIGHT:
            pass
        elif symbol == arcade.key.UP:
            pass
        elif symbol == arcade.key.DOWN:
            pass
