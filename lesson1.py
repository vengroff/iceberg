# Lesson 1 - using arcade.

import arcade

# The first thing we have to do is open a window and set the
# background color.
arcade.open_window('Lesson 1', 1000, 900)
arcade.set_background_color(arcade.color.WHITE)

# To let arcade know we are drawing in the window,
# we use the start_render() function as follows.
# When we are done we will use finish_render() to
# finish up.
arcade.start_render()

# Now we do all our drawing.

# Lines:
arcade.draw_line(100, 100, 700, 500, arcade.color.RED)
arcade.draw_line(100, 500, 700, 100, arcade.color.BLUE)

# Text:
arcade.draw_text("spiderman ", 100, 300, arcade.color.RED, 30)

# Rectangles:
arcade.draw_rectangle_filled(400, 100, 60, 120, arcade.color.LIGHT_GRAY);
arcade.draw_rectangle_outline(400, 100, 60, 120, arcade.color.BLACK);

# Polygons.
points = ((600, 300), (550, 250), (600, 325), (500, 275), (650, 350))
arcade.draw_polygon_filled(points, arcade.color.YELLOW)
arcade.draw_polygon_outline(points, arcade.color.PURPLE, 4)

#triangle function
def triangle(x,y, size):
    print(x,y)
    points = ((x,y+size), (x,y), (x+size,y))
    arcade.draw_polygon_filled(points, arcade.color.BLUE);
    arcade.draw_polygon_outline(points, arcade.color.PURPLE, 4);
    points = ((x,y-size), (x,y), (x+size,y))
    arcade.draw_polygon_filled(points, arcade.color.BLUE);
    arcade.draw_polygon_outline(points, arcade.color.PURPLE, 4);

for x in range(200,600,100):
    print (x)
    triangle(x,100,100)



# Circles
arcade.draw_circle_filled(400, 500, 80, arcade.color.LIGHT_GRAY);
arcade.draw_circle_outline(400, 500, 80, arcade.color.DARK_CANDY_APPLE_RED, 5);

# A flower made of ellipses.
for angle in range(0, 360, 10):
    arcade.draw_ellipse_outline(400, 300, 100, 20, arcade.color.AFRICAN_VIOLET, tilt_angle=angle)

arcade.draw_circle_filled(400, 300, 20, arcade.color.PURPLE);


# TODO - explore drawing additional shapes at various locations.
# Explore additional functions such as
#
#    arcade.draw_arc_outline(...)
#    arcade.draw_arc_filled(...)
#    arcade.draw_line_strip(...)
#
# Then move on to lesson1-art.py.

# We are finished drawing...
arcade.finish_render()

# Show the drawing in our window.
arcade.run()

