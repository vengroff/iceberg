# Lesson 0 - Icebergs, Bridges, Cloning, and A Few Lines of Code

- Introductions
  - Instructor introductions
  - Student Introductions 
  	 - 2min, 
  	 - Name, 
  	 - [age], 
  	 - [school], 
  	 - coded before?, 
  	 - favorite food, 
  	 - something funny.
- Iceberg analogy.

- Building bridges.

- Repositories
  - Look at repo on web.
  - See README.md. Pause and read through sections on icebergs and bridges for reinforcement.
  - Cloning. 
  - 	Each student clones to their local environment.

- Opening the shell / Hello, World.
  - All students open.
  - print "Hello, World."
  - You just shot the arrow across the ravine!
  - Where are the ropes?
    - cd to the right place
    - from lifeboard import Board, color
    - variables/assignment with =
      - b = Board()
    - method call
      - b.setColor(1, 1, color.red)
    - print color.colors
    - Use colors to draw something manually.
      - Line
      - Smiley face



        

    


   

  
   