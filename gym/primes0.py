
import time


def is_in(value, array):
    for ii in range(len(array)):
        if value == array[ii]:
            return True

    return False


def sieve(n: int):
    """
    Simple sieve of Eratosthenes to find prime numbers.
    :param n: Look for primes up to and including this number.
    :return: A list of primes less than n.
    """

    # numbers is a list of all the numbers between
    # 2 and n - 1 that might be prime, that is the numbers
    # we have not determined are composite yet.
    numbers = list(range(2,n))

    # Look at each ii that we want to eliminate multiples of:
    for ii in range(2,n):
        # Eliminate the multiples of ii
        for composite in range(2*ii, n, ii):
            # If we have not already removed it, remove it now.
            # if is_in(composite, numbers):
            if composite in numbers:
                numbers.remove(composite)

    # Now all that is left are the primes
    return numbers


def sieve1(n: int):
    """
    Faster sieve of Eratosthenes to find prime numbers.
    :param n: Look for primes up to and including this number.
    :return: A list of primes less than n.
    """

    # is_prime is an array of booleans (True or False)
    # indicating whether numbers are prime. If x is prime
    # then we want to end up with is_prime[x] == True,
    # and if it is composite, we want is_prime[x] == False.

    # Initialize the array to have n elements, all True.
    is_prime = [True] * n

    # 0 and 1 are composite
    is_prime[0] = False
    is_prime[1] = False

    # Look at each ii that we want to eliminate multiples of:
    for ii in range(2,n):
        # Eliminate the multiples of ii
        for composite in range(2*ii, n, ii):
            is_prime[composite] = False

    # Now construct a list of primes as a list of
    # all the ii's less than n which we did not
    # mark as composite.
    primes = [ii for ii in range(n) if is_prime[ii]]

    return primes



def sieve2(n: int):
    """
    Even faster sieve of Eratosthenes to find prime numbers.
    It starts checking composites at ii*ii instead of 2*ii.
    :param n: Look for primes up to and including this number.
    :return: A list of primes less than n.
    """

    # is_prime is an array of booleans (True or False)
    # indicating whether numbers are prime. If x is prime
    # then we want to end up with is_prime[x] == True,
    # and if it is composite, we want is_prime[x] == False.

    # Initialize the array to have n elements, all True.
    is_prime = [True] * n

    # 0 and 1 are composite
    is_prime[0] = False
    is_prime[1] = False

    # Look at each ii that we want to eliminate multiples of:
    for ii in range(2,n):
        # Eliminate the multiples of ii
        for composite in range(ii*ii, n, ii):
            is_prime[composite] = False

    # Now construct a list of primes as a list of
    # all the ii's less than n which we did not
    # mark as composite.
    primes = [ii for ii in range(n) if is_prime[ii]]

    return primes



def main():
    # Choose a large n
    n = 10000

    # Time the original version of the code:
    start0 = time.clock()
    primes = sieve(n)
    end0 = time.clock()

    elapsed0 = end0 - start0

    # Time the improved version of the code.
    start1 = time.clock()
    primes1 = sieve1(n)
    end1 = time.clock()

    elapsed1 = end1 - start1

    # Print our results.
    print("sieve0 found {} primes less than {} in {}".format(len(primes), n, elapsed0))
    print("sieve1 found {} primes less than {} in {}".format(len(primes1), n, elapsed1))
    print("Speedup: {}".format(elapsed0 / elapsed1))

    if False:
        print("The primes that are less than {} are:".format(n))

        for prime in primes1:
            print(prime)


# This is the standard Python idiom to run
# a main function if this file is run directly
# from either the IDE or the command line.
if __name__ == "__main__":
    main()
