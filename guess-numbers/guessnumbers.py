
import random
import math
import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MIN_NUMBER = 0
MAX_NUMBER = 999


class NumbersWindow(arcade.Window):
    """An arcade window with a number guessing game."""

    # States the game can be in.
    (START_SCREEN, ENTERING_NUMBER, CHECK_NUMBER, GAME_OVER) = range(4)

    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT)
        arcade.set_background_color(arcade.color.WHITE)
        self.state = NumbersWindow.START_SCREEN

    def start_new_game(self):
        """ Set up the game and initialize the variables. """
        self.state = NumbersWindow.START_SCREEN
        self.guess = None
        self.guessesLeft = 10
        # Chose a random number that we have to try to guess.
        self.actualNumber = random.randint(MIN_NUMBER, MAX_NUMBER)
        self.lowerBound = MIN_NUMBER
        self.upperBound = MAX_NUMBER

    def on_draw(self):
        """
        Draw everything we need on the screen. What we draw
        depends on the state we are in.
        """

        arcade.start_render()

        if self.state == NumbersWindow.START_SCREEN:
            # Render the start screen
            arcade.draw_text('Let\'s play a number guessing game.', 0, 3 * SCREEN_HEIGHT / 4, arcade.color.RED, 36,
                             SCREEN_WIDTH, "center", bold=True)
            arcade.draw_text('Hit spacebar to start.', 0, SCREEN_HEIGHT / 2, arcade.color.RED, 36,
                             SCREEN_WIDTH, "center", bold=True)
        elif self.state == NumbersWindow.GAME_OVER:
<<<<<<< Updated upstream
            # Tell the user the game is over. If they guessed the right number,
            # then tell then congratulate them.
            pass
=======
            if self.guess is not None and self.guess == self.actualNumber:
                arcade.draw_text("Congratulations.\nYou guessed {} correctly\nwith {} guesses left.".format(
                    self.actualNumber, self.guessesLeft),
                    0, 2 * SCREEN_HEIGHT / 3, arcade.color.RED, 24, SCREEN_WIDTH, "center", bold=True)
            else:
                arcade.draw_text('GAME OVER', 0, 2 * SCREEN_HEIGHT / 3, arcade.color.RED, 48,
                                 SCREEN_WIDTH, "center", bold=True)
                arcade.draw_text('Number was {}'.format(self.actualNumber), 0, SCREEN_HEIGHT / 2, arcade.color.RED, 24,
                                 SCREEN_WIDTH, "center", bold=True)

            arcade.draw_text('Hit any key to continue.', 0, SCREEN_HEIGHT / 3, arcade.color.RED, 36,
                             SCREEN_WIDTH, "center", bold=True)
>>>>>>> Stashed changes
        elif self.state == NumbersWindow.ENTERING_NUMBER or self.state == NumbersWindow.CHECK_NUMBER:
            # There are certain things we need to render in either of these states, like
            # the current guess and the number of guesses left. Do that here.

            if self.state == NumbersWindow.ENTERING_NUMBER:
<<<<<<< Updated upstream
                # We need to render a message indicating the min and max numbers we accept.
                # We also need to tell the user they need to hit enter to check their guess.
                pass
            elif self.state == NumbersWindow.CHECK_NUMBER:
                # Check if the number is too high or too low and alert the user.
                pass
=======
                arcade.draw_text("Enter a number between {} and {}\nthen hit the enter key to check it.".format(MIN_NUMBER, MAX_NUMBER),
                                 0, SCREEN_HEIGHT / 3, arcade.color.RED, 24, SCREEN_WIDTH, "center", bold=True)
                arcade.draw_text("For a hint, hit the H key.",
                                 0, 2 * SCREEN_HEIGHT / 9, arcade.color.PURPLE, 12, SCREEN_WIDTH, "center")
            elif self.state == NumbersWindow.CHECK_NUMBER:
                if self.guess < self.actualNumber:
                    if self.guess >= self.lowerBound:
                        self.lowerBound = self.guess + 1
                    arcade.draw_text('{} is too low. Hit enter to try again.'.format(self.guess),
                                     0, SCREEN_HEIGHT / 3, arcade.color.RED, 24, SCREEN_WIDTH, "center", bold=True)
                elif self.guess > self.actualNumber:
                    if self.guess <= self.upperBound:
                        self.upperBound = self.guess - 1
                    arcade.draw_text('{} is too high. Hit enter to try again.'.format(self.guess),
                                     0, SCREEN_HEIGHT / 3, arcade.color.RED, 24, SCREEN_WIDTH, "center", bold=True)
            arcade.draw_text('{} guesses left.'.format(self.guessesLeft),
                                     0, SCREEN_HEIGHT / 6, arcade.color.DARK_GREEN, 18, SCREEN_WIDTH, "center", bold=True)
>>>>>>> Stashed changes

    def on_key_press(self, symbol: int, modifiers: int):
        """Handle key presses for each state we might be in."""
        if self.state == NumbersWindow.START_SCREEN:
            if symbol == arcade.key.SPACE:
                # Start ENTERING_NUMBER.
                self.state = NumbersWindow.ENTERING_NUMBER
        elif self.state == NumbersWindow.GAME_OVER:
            # Go back to the start screen on any key.
            self.start_new_game()
        elif self.state == NumbersWindow.ENTERING_NUMBER:
            if symbol >= ord('0') and symbol <= ord('9'):
                digit = symbol - ord('0')
                # TODO - change self.guess to have the new digit.
                pass
            elif symbol == arcade.key.ENTER or symbol == arcade.key.RETURN:
<<<<<<< Updated upstream
                # TODO - Lower the number of guesses left and go to the check
                # number state.
                pass
            elif symbol == arcade.key.DELETE or symbol == arcade.key.BACKSPACE:
                # TODO - Delete the last digit.
                pass
=======
                if self.guess is not None:
                    self.guessesLeft -= 1
                    if self.guessesLeft <= 0:
                        self.state = NumbersWindow.GAME_OVER
                    elif self.guess == self.actualNumber:
                        self.state = NumbersWindow.GAME_OVER
                    else:
                        self.state = NumbersWindow.CHECK_NUMBER
            elif symbol == arcade.key.DELETE or symbol == arcade.key.BACKSPACE:
                if self.guess is not None:
                    self.guess = int(self.guess / 10)
                    if self.guess == 0:
                        self.guess = None
            elif symbol == arcade.key.H:
                self.guess = int((self.lowerBound + self.upperBound) / 2)
>>>>>>> Stashed changes
        elif self.state == NumbersWindow.CHECK_NUMBER:
            if symbol == arcade.key.ENTER or symbol == arcade.key.RETURN:
                # TODO - start the next guess.
                pass

def main():
    window = NumbersWindow()
    window.start_new_game()
    arcade.run()

if __name__ == '__main__':
    main()
