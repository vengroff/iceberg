## Introduction to Data Types

We have been using data types in our games, but we haven't really formally talked about them yet. So we are going to go back into the weight room and work with them a little bit.

### Lists

We have been using lists quite a bit. A list is just a sequence of items, like numbers or strings. 

#### Constructing a List

We can construct lists as follows:

```python
# Create some lists:

a = []
b = [ 10, 20, 30 ]

print(a)
print(b)
```

Try this in your IDE. Run your code and make sure it prints the contents of the lists. 

Note that `a` is an empty list and `b` has three elements. We can also check their size with code. Try this:

```python
# How big are the lists?

print(len(a))
print(len(b))
```
**Exercise 1:** Try some examples on your own. Make some bigger and smaller lists. Print them, check their sizes, and just generally get comfortable with using them.

#### Getting Elements out of Lists

Next, we can get elements from lists. Try this, assuming you have already created the lists `a` and `b` as above.

```python
# Get an element out of a list:

x = b[0]
y = b[1]

print("x:", x)
print("y:", y)
```

Try a few others. Remember that the first element is at index `0`, not `1`.

**Exercise 2:** What happens if you try to get an element out of the list that is beyond the maximum? What happens if you try to use a negative index?

#### Checking for a Value in a List

```python
# Does an array contain a given element?

c20 = 20 in b
c50 = 50 in b

print("c20:", c20)
print("c50:", c50)
```

**Exercise 3:** Write some code that checks if a value `q` is in `b` and sets `f = b` if it is there and `f = -1` if it is not. Then print `f`.

**** Adding to an Existing List

We saw how to construct a complete array with `[]` containing zero or more elements. What if we want to add more? Try this, assuming you have the empty `a` from before:

```python
# Add an element to an array:

print("a was:", a)
a.append(500)
print("a is :", a)

for ii in range(0, 10000, 1000):
    a.append(ii)

l = len(a)

print("Now, a has", l, "elements.")
print("a:", a)
```

**** Iterating Through a List

We can look at every element of a list with a for loop as follows:

```python
for x in a:
    print(x)
```

We will do the body of the loop once for each element of `a`. Inside the body, that value will be named `x` and we can do whatever we want with it.

**Exercise 4:** Write some code to iterate through the elements of the list `b` and append each one of them to `a`.

#### Lists Aren't Just for Numbers

Try out the following code, which puts some things other than numbers into arrays.

```python
# Arrays can have more than numbers in them.

ar1 = [ "An array can contain...", "strings" ]
print("ar1:", ar1)

ar2 = [ "An array can contain...", 17, "a mix of things."]

for thing in ar2:
    print(thing)

ar3 = [ "An array can contain...", [ 10, 20, 30], "another array!" ]
print("ar3:", ar3)

# We can even put arrays inside arrays inside arrays...

aa = [ 1, [ 2, [ 132, [ "Hi!", [ ar3, [  ]]]]]]

print("aa", aa)
```

Does this make sense? Try some examples on your own.

**Exercise 5:** Write some code to construct lists with different types of elements. Try using `append()`, not just the square brackets as shown above.

#### Sorting Arrays

```python
# What else can we do with arrays? We can sort them.

a10 = [ 17, 51, 3, 97, 13 ]
a10s = sorted(a10)

print("Sorted: ", a10s)
```

We can also sort in reverse.

```python
# We can also sort in reverse:

a10r = sorted(a10, reverse=True)

print("Reversed: ", a10r)
```

**Exercise 6:** What happens if we try to sort a list containing both numbers and strings?. What about if we sort a list containing numbers and other lists?

### Dictionaries

Lists are good for a lot of things, but sometimes we want a data structure that lets us give names (also called keys) to values. In Python this data structure is called a dictionary.

#### Constructing a Dictionary

The easiest way to see a dictionary is action is to create one.

```python
# A dictionary is like an array, but instead of an integer for
# an index, we use a string.

# For example, we could use a dictionary to keep track
# of how many of each item we have in a store. We use curly
# braces {} to define a dictionary. Inside, we put each key
# in as a string, followed by a colon (:) and a value. We
# separate the key/value pairs with commas. So a dictionary
# with two keys looks like this:
inventory = { "widgets": 10, "gadgets": 15 }

print("Inventory:", inventory)
```

#### Getting elements out of a Dictionary

Getting an element out of a dictionary looks a lot like getting an element out of a list, except that instead of using a number
for an index, we use a string. As follows:

```python
w = inventory['widgets']
g = inventory['gadgets']

print("We have ", w, "widgets and", g, "gadgets")
```

#### Adding Elements to a Dictionary

Adding to a Dictionary also uses the square brackets. For example:

```python
# We can add items to the dictionary

inventory['toys'] = 12
inventory['games'] = 5

# Notice that when we print dictionaries, the 
# keys can come out in any order. Unlike lists,
# the keys are not ordered. We will have to sort
# them if we want order. More about that later.
print("Inventory:", inventory)
```

**Exercise 7:** Write some code that starts with an empty dictionary and adds some key, value pairs to it.

**Exercise 8:** What does this code do? Why?

```python
names = [ 'apple', 'banana', 'grape', 'mango', 'pizza']

places = {}

for ii in range(len(names)):
    places[names[ii]] = ii

print("Places: ", places)
```

#### What Keys Does a Dictionary Have?

We may not always know what keys a dictionary has. Luckily, we can ask it as follows:

```python
# What if we don't know what all the keys are?
# We can ask the dictionary.

k = inventory.keys()

print("Inventory keys:", k)

# More commonly, we want to do something with each:
for k in inventory:
    print(k)
```

**Exercise 9:** Write a version of the code above that prints each key and its associated value.

#### Doing more with Keys

Inside the `for` loop over the keys in a dictionary, we can do a lot more than get values and print them. We can do all kinds of things, including conditional code like this, which checks whether we are low on certain items.

```python
for k in inventory:
    if inventory[k] < 10:
        print("We are low on", k)
```

If this inventory dictionary is part of a system we are using to run a small store, we might want to compare how much inventory we have of each item in the store vs. the demand we expect from our customers this week. This would look something like this (assuming we already have `inventory` constructed as above.

```python
# Perhaps we have two dictionaries, one with predicted demand
# and one with inventory:

demand = { "widgets": 20, "gadgets": 10, "toys": 7, "games": 20 }

for k in demand:
    if inventory[k] < demand[k]:
        print("We don't have enough", k)
```

**Exercise 10:** Write a version of the code above that prints the names of the items we have enough of.

#### Dealing with Missing Items

Sometimes, especially when we are dealing with more than one dictionary, a key will exist in one but not the other. Here is one way we can deal with situations like that:

```python
demand['apples'] = 100

for k in sorted(demand):
    if k in inventory:
        if inventory[k] < demand[k]:
            print("We don't have enough", k)
        else:
            print("We have enough", k)
    else:
        print("We don't have any", k)
```

**Exercise 11:** Write your own version of this that tells us how many of each item we need to order, i.e. `demand[k] - inventory[k]` if we don't have enough inventory or how many extra we will have, i.e. `inventory[k] - demand[k]` if we do have enough.

### Filtering

Let's go back to lists for a moment. One way we can create a list is by filtering another list. This means taking every element of the list that meets some criterion and then putting it (or something computed from it) into the resulting list. 

Here is an example

```python
an = [ 2, 17, 8, 0, 4, 92, -13, 14, 3 ]
af = [ x for x in an if x < 10 ]
print("af:", af)
```

**Exercise 12:** Write your own version of this that builds an array of all the elements of an that are not negative. 

#### Filtering Keys of a Dictionary

The keys of a dictionary are like a list, so we can do something just like the code in the last section with this

```python
missing = [item for item in demand if item not in inventory]
enough  = [item for item in demand if item in inventory and inventory[item] >= demand[item]]
insufficient = [item for item in demand if item in inventory and inventory[item] < demand[item]]

print("Missing     :", sorted(missing))
print("Insufficient:", sorted(insufficient))
print("Enough      :", sorted(enough))
```

The syntax takes a little getting used to, but it is very powerful once you understand it.

### More Exercises:

Let's imagine that in addition to inventory and demand, we have prices of items in a dictionary like this:

```python
prices = {
    "apples": 0.49,
    "games" : 5.99,
    "widgets" : 1.79,
    "gadgets" : 7.99,
    "toys" : 1.29
}
```

Now we have enough information to write some interesting reports for a store. For example, we can see what the total revenue would be if we could meet all demand

```python
# If we sell exactly the demand, how much revenue will we have?

demand_revenue = 0.00

for item in demand:
    demand_revenue = demand_revenue + demand[item] * prices[item]

print("Demand revenue    : {:.2f}".format(demand_revenue))
```

Notice that we use a special new `format()` function that only appears in the newest versions of python to print our demand to 
two decimal places. We'll look at `format()` another time. It is powerful, but sometimes non-intuitive.

**Exercise 13:** Modify the demand revenue code above to limit revenue to the inventory we have if we don't have enough inventory to meet demand. Make sure to deal with items that are missing from any of the dictionaries you use if you need to. Report to the user how much revenue they are losing because they don't have adequate inventory to meet demand.

